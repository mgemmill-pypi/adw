adw v0.1.1
==========

``adw`` is a convenience wrapper around the appdirs library.

It's primarily doing three things:

    1. all directory attributes are python3 pathlib.Path objects.
    2. breaking site and user directory paths into separate objects.
    3. provides a convenient `assert_directories` method.


##### Usage


    import adw

    dirs = adw.AppDirs('myapp')

    # dirs are allocated to separate site and user objects
    print(dirs.user.data_dir)
    print(dirs.site.data_dir)

    # create the applications directories if they don't exist
    dirs.user.assert_directories()

    # use new pathlib functionality
    data = dirs.user.data_dir / 'myapp.data'
